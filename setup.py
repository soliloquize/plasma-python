# -*- encoding:utf-8 -*-

from setuptools import setup, find_packages

setup(
    name="plasma",
    version="0.1",
    packages=find_packages(exclude=['tests', 'sources', 'sources.*']),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Jinja2==2.8',
        'Markdown==2.6.2',
        'MarkupSafe==0.23',
        'PyYAML==3.11',
        'Pygments==2.0.2',
        'wsgiref==0.1.2',
    ],
    scripts=['plasma-admin.py'],
    author='numerical',
)
