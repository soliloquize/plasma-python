# -*- encoding:utf-8 -*-


from base_test import BaseTestCase
from core.workflow import Workflow


class WorkflowTest(BaseTestCase):

    def test_execute(self):
        settings = self.build_settings()
        settings['SOURCE_FOLDER'] = '../sources/posts'
        workflow = Workflow()
        workflow.execute(settings)
