# -*- encoding:utf-8 -*-


from base_test import BaseTestCase
from utils.datetime_utils import reformat


class DatetimeUtilsTest(BaseTestCase):

    def test_reformat(self):
        result = reformat('2015-07-07 22:36:26', '%Y-%m-%d %H:%M:%S', '/%Y/%m/%d/')
        self.assertEqual('/2015/07/07/', result)
