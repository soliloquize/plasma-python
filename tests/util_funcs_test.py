# -*- encoding:utf-8 -*-

from base_test import BaseTestCase
from utils.util_funcs import escape_char


class UtilFuncsTest(BaseTestCase):

    def test_escape_char(self):
        result = escape_char('Java 8中的函数式编程（一）()：；～！@#￥%……&*)(*&^%$#@!')
        self.assertEqual(u'Java-8中的函数式编程一', result)

        result = escape_char('设计一个基于Python的静态博客')
        self.assertEqual(u'设计一个基于Python的静态博客', result)