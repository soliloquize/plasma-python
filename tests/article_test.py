# -*- encoding:utf-8 -*-

from base_test import BaseTestCase
from core.article import Article


class ArticleTest(BaseTestCase):

    def test_build_url_str(self):
        path = u'Java-8中的函数式编程一'
        date = u'2015-07-07 22:36:26'
        result = Article.build_url_str(date, path)
        print result
        self.assertEqual(u'/2015/07/07/Java-8中的函数式编程一', result)
