# -*- encoding:utf-8 -*-

from base_test import BaseTestCase
from parsers.parsers_mgr import Parsers


class ParsersTest(BaseTestCase):

    def test_parse(self):
        settings = self.build_settings()
        parsers = Parsers(settings)
        result = parsers.parse()
        self.assertTrue(result)
        article = result.values()[0]
        self.assertEqual(u'技术', article.categories)