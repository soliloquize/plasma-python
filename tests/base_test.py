# -*- encoding:utf-8 -*-

from unittest import TestCase


MARKDOWN_EXTENSIONS = [
    'markdown.extensions.fenced_code',
    'markdown.extensions.codehilite',
    'markdown.extensions.footnotes',
    'markdown.extensions.meta',
    'markdown.extensions.nl2br',
    'markdown.extensions.sane_lists',
    'markdown.extensions.toc',
    'markdown.extensions.wikilinks',
    'markdown.extensions.tables'
]


class BaseTestCase(TestCase):

    def build_settings(self):
        return {
            'THEME_FOLDER': '..plasma/static/themes',
            'THEME': 'bootstrap',
            'SOURCE_FOLDER': './data',
            'PUBLIC_FOLDER': './output',
            'PAGE_NUM': 10,
            'MARKDOWN_EXTENSIONS': MARKDOWN_EXTENSIONS,
            'BAIDU': ''}
