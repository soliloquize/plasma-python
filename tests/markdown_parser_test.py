# -*- encoding:utf-8 -*-

from base_test import BaseTestCase
from parsers.markdown_parser import MarkdownParser


class MarkdownParserTest(BaseTestCase):

    def test_parse_meta(self):
        path = 'data/2015-10-25-sample.md'
        parser = MarkdownParser(self.build_settings())
        meta, html = parser.parse(path)
        self.assertEqual(2, len(meta['tags']))
        print meta['title']
