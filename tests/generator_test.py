# -*- encoding:utf-8 -*-

from base_test import BaseTestCase
from parsers.parsers_mgr import Parsers
from generator.generators import Generators
from jinja2 import Template


class GeneratorTest(BaseTestCase):

    def test_jinja2_template(self):
        template = Template('Hello {{ name }}!')
        res = template.render(name="foobar")
        self.assertEqual('Hello foobar!', res)

        content = """
            {% set local_var = 0 %}
            {% for item in item_list %}
                {% if local_var != item.value %}
                    <span>{{ item.value }}</span>
                    {% set local_var = item.value %}
                {% else %}
                    <span>foobar</span>
                {% endif %}
            {% endfor %}
        """
        template = Template(content)
        item_list = [
            {'value': 1},
            {'value': 2},
            {'value': 2},
            {'value': 3},
            {'value': 3},
            {'value': 3},
        ]
        res = template.render(item_list=item_list)
        print res

    def test_get_template(self):
        settings = self.build_settings()
        generator = Generators(settings)
        template = generator.env.get_template('base.html')
        self.assertIsNotNone(template)
        template = generator.env.get_template('article.html')
        self.assertIsNotNone(template)
        res = template.render(article=self.build_article(), settings={})
        self.assertIsNotNone(res)

    def build_article(self):
        settings = self.build_settings()
        parsers = Parsers(settings)
        result = parsers.parse()
        article = result.values()[0]
        return article
