# -*- encoding:utf-8 -*-

SITE_NAME = ''  # 'SOLILOQUIZE'
SITE_URL = ''  # 'http://blog.soliloquize.org'

AUTHOR = ''  # 'numerical'

THEME = 'bootstrap'

THEME_FOLDER = '{themes}'
PUBLIC_FOLDER = '{base}/public'

SOURCE_FOLDER = '{base}/sources/posts'
IMAGE_FOLDER = '{base}/sources/public_image'
FILE_FOLDER = '{base}/sources/public_file'


SERVER_NAME = ''
REMOTE_PATH = ''
IS_BOOK = False


LANG = 'zh'

PAGE_NUM = 10

STATS_TOKEN = ''

BAIDU = ''

MARKDOWN_EXTENSIONS = [
    'markdown.extensions.fenced_code',
    'markdown.extensions.codehilite',
    'markdown.extensions.footnotes',
    'markdown.extensions.meta',
    'markdown.extensions.nl2br',
    'markdown.extensions.sane_lists',
    'markdown.extensions.toc',
    'markdown.extensions.wikilinks',
    'markdown.extensions.tables'
]
