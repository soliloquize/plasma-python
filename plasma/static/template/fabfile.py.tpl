# -*- encoding:utf-8 -*-

from __future__ import with_statement
from fabric.api import run, cd, hosts, execute


@hosts('localhost')
def add_and_commit():
    with cd('{work_dir}'):
        run('rm -r public')
        run('plasma-admin.py generate')
        run('git add .')
        run('git commit -am "add content"')
        run('git push -u origin master')


@hosts('{server_name}')
def update_remote_server():
    with cd('{remote_path}'):
        run('git pull')


def publish():
    execute(add_and_commit)
    execute(update_remote_server)
