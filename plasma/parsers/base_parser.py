# -*- encoding:utf-8 -*-

from plasma.utils import encoding_utils


class BaseParser(object):

    def __init__(self, settings):
        self.settings = settings

    def parse(self, path):
        return self.do_parse(self.read(path))

    def read(self, path):
        with open(path) as inputs:
            return encoding_utils.ensure_unicode(inputs.read())

    def is_accept(self, ext):
        return ext.lower() in self.get_file_extensions()

    def get_file_extensions(self):
        return []

    def do_parse(self, content):
        raise NotImplemented
