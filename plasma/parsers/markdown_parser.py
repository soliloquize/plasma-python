# -*- encoding:utf-8 -*-

import markdown
import re
from plasma.parsers.base_parser import BaseParser


class MarkdownParser(BaseParser):

    def __init__(self, settings):
        super(MarkdownParser, self).__init__(settings)

    def get_file_extensions(self):
        return ['.md', '.markdown']

    def do_parse(self, content):
        md = markdown.Markdown(extensions=self.settings['MARKDOWN_EXTENSIONS'])
        html = md.convert(content)
        meta = self._parse_meta(md.Meta)
        return meta, html

    def _parse_meta(self, meta):
        result = {}
        for key, value in meta.iteritems():
            if not value:
                continue
            multi = re.match('\\[(.*?)\\]', value[0])
            if not multi:
                result[key] = self.escape_quote(value[0])
            else:
                result[key] = [self.escape_quote(x) for x in multi.group(1).split(',')]
        return result

    def escape_quote(self, content):
        content = content.strip()
        if content.startswith("'") and content.endswith("'"):
            return content[1:-1]
        if content.startswith('"') and content.endswith('"'):
            return content[1:-1]
        return content