# -*- encoding:utf-8 -*-

from plasma.parsers.markdown_parser import MarkdownParser
from plasma.core.article import Article
import os


class Parsers(object):

    def __init__(self, settings):
        self.parser_list = [MarkdownParser(settings)]
        self.settings = settings

    def parse(self):
        result = {}
        folder = self.settings['SOURCE_FOLDER']
        for base, folders, names in os.walk(folder):
            for name in names:
                path = os.path.join(base, name)
                ext = path[path.rindex('.'):]
                for parser in self.parser_list:
                    if not parser.is_accept(ext):
                        continue
                    meta, html = parser.parse(path)
                    article = Article.build(meta, html)
                    result[article.url] = article
        return result
