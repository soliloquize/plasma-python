# -*- encoding:utf-8 -*-

from plasma.generator.generators import Generators
from plasma.parsers.parsers_mgr import Parsers


class Workflow(object):

    def execute(self, settings):
        generators = Generators(settings)
        parsers = Parsers(settings)
        result = parsers.parse()
        generators.generate(result)
