# -*- encoding:utf-8 -*-

from plasma.utils import util_funcs
from plasma.utils import encoding_utils
from plasma.utils import datetime_utils
from plasma.utils.encoding_utils import ensure_unicode


class Article(object):

    def __init__(self):
        self.title = None
        self.tags = None
        self.categories = None
        self.date = None
        self.comment = True
        self.draft = False
        self.url = None
        self.slug = None
        self.content = None
        self.identifier = None
        self.not_in_list = False

    @staticmethod
    def build(meta, content):
        article = Article()
        util_funcs.init_from_dict(article, meta)
        article.identifier = hash(ensure_unicode(article.slug))
        article.content = content
        article.url = Article.build_url_str(article.date, util_funcs.escape_char(article.title))
        if isinstance(article.categories, list):
            article.categories = article.categories[0]
        if not isinstance(article.tags, list) and article.tags:
            article.tags = [article.tags]
        return article

    @staticmethod
    def build_url_str(date, title):
        date = encoding_utils.ensure_unicode(date)
        date = datetime_utils.reformat(date, '%Y-%m-%d %H:%M:%S', '/%Y/%m/%d/')
        return date + encoding_utils.ensure_unicode(title)


