# -*- encoding:utf-8 -*-

from plasma.generator.article_list_generator import ArticleListGenerator
from collections import defaultdict


class TagsGenerator(ArticleListGenerator):

    def __init__(self, env, settings):
        super(TagsGenerator, self).__init__(env, settings)

    def build_group_dict(self, article_list):
        result = defaultdict(list)
        for article in article_list:
            for tag in article.tags:
                result[tag].append(article)
        return result

    def get_template_name(self):
        return 'tags.html'

    def get_base(self):
        return 'tags'