# -*- encoding:utf-8 -*-

from plasma.generator.article_list_generator import ArticleListGenerator
from plasma.utils.datetime_utils import reformat


class AtomGenerator(ArticleListGenerator):

    def __init__(self, env, settings):
        super(AtomGenerator, self).__init__(env, settings)
        self.timestamp = None

    def build_group_dict(self, article_list):
        self.timestamp = article_list[0].date
        return {'': article_list[:self.settings['PAGE_NUM']]}

    def get_kwargs(self):
        self.timestamp = reformat(self.timestamp, '%Y-%m-%d %H:%M:%S', '%Y-%m-%dT%H:%M:%S+08:00')
        return {'timestamp': self.timestamp}

    def get_template_name(self):
        return 'atom.html'

    def get_base(self):
        return 'atom.xml'