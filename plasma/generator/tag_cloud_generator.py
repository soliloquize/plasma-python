# -*- encoding:utf-8 -*-

from plasma.generator.base_generator import BaseGenerator
from collections import Counter
from plasma.utils.util_funcs import escape_char


class TagCloudGenerator(BaseGenerator):
    def __init__(self, env, settings):
        super(TagCloudGenerator, self).__init__(env, settings)

    def generate(self, article_list):
        folder = 'tags'
        total = 0
        tag_info = Counter()
        for article in article_list:
            for tag in article.tags:
                tag_info[tag] += 1
                total += 1
        tag_list = []
        avg = total * 3.0 / len(tag_info)
        for key, value in tag_info.iteritems():
            tag_list.append(
                {
                    'name': key,
                    'url': '/tags/%s/' % escape_char(key),
                    'num': value,
                    'size': min(8 + (1.0 * value / avg) * 15, 40),
                }
            )
        self.render(folder, tag_list=tag_list)

    def get_template_name(self):
        return 'tag_cloud.html'
