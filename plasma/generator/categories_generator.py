# -*- encoding:utf-8 -*-

from plasma.generator.article_list_generator import ArticleListGenerator
from collections import defaultdict


class CategoriesGenerator(ArticleListGenerator):

    def __init__(self, env, settings):
        super(CategoriesGenerator, self).__init__(env, settings)

    def build_group_dict(self, article_list):
        result = defaultdict(list)
        for article in article_list:
            result[article.categories].append(article)
        return result

    def get_template_name(self):
        return 'categories.html'

    def get_base(self):
        return 'categories'