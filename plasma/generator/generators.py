# -*- encoding:utf-8 -*-

from jinja2 import Environment
from jinja2 import FileSystemLoader
from jinja2 import ChoiceLoader
from plasma.generator.article_generator import ArticleGenerator
from plasma.generator.index_generator import IndexGenerator
from plasma.generator.tags_generator import TagsGenerator
from plasma.generator.categories_generator import CategoriesGenerator
from plasma.generator.tag_cloud_generator import TagCloudGenerator
from plasma.generator.atom_generator import AtomGenerator
from plasma.utils.datetime_utils import reformat
from plasma.utils import util_funcs
import os
import shutil


class Generators(object):

    def __init__(self, settings):
        self.settings = settings
        self.env = Environment(
            trim_blocks=True,
            lstrip_blocks=True,
            loader=ChoiceLoader([
                FileSystemLoader(os.path.join(settings['THEME_FOLDER'], settings['THEME'], 'templates'))
            ]),
            extensions=[],
        )
        self.env.filters.update({
            'escape_char': escape_char,
            'datetime_format': datetime_format,
            'to_str': to_str,
        })

        self.article_generator = ArticleGenerator(self.env, settings)
        self.index_generator = IndexGenerator(self.env, settings)
        self.tags_generator = TagsGenerator(self.env, settings)
        self.categories_generator = CategoriesGenerator(self.env, settings)
        self.tag_cloud_generator = TagCloudGenerator(self.env, settings)
        self.atom_generator = AtomGenerator(self.env, settings)

    def generate(self, article_dict):
        article_list = article_dict.values()
        article_list.sort(key=lambda x: x.date, reverse=True)
        if self.settings['IS_BOOK']:
            self.article_generator.generate(article_list)
            self.index_generator.generate(self.filter_not_in_list(article_list))
            self.atom_generator.generate(article_list)
        else:
            self.article_generator.generate(article_list)
            self.index_generator.generate(self.filter_not_in_list(article_list))
            self.tags_generator.generate(article_list)
            self.categories_generator.generate(article_list)
            self.tag_cloud_generator.generate(article_list)
            self.atom_generator.generate(article_list)
        self.copy_res()

    def filter_not_in_list(self, article_list):
        return [x for x in article_list if not x.not_in_list]

    def copy_res(self):
        dst = os.path.join(self.settings['PUBLIC_FOLDER'], 'themes')
        if os.path.exists(dst):
            shutil.rmtree(dst)
        src = self.settings['THEME_FOLDER']
        shutil.copytree(src, dst)


def datetime_format(value, src_format='%Y-%m-%d %H:%M:%S', dst_format='%Y-%m-%d'):
    return reformat(value, src_format, dst_format)


def escape_char(value):
    return util_funcs.escape_char(value)


def to_str(value):
    return str(value)
