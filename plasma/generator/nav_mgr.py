# -*- encoding:utf-8 -*-
import os
from plasma.utils.cache import cache
from plasma.utils.encoding_utils import ensure_unicode


class NavItem(object):
    def __init__(self, path, href, name):
        self.path = path.split('/')[1:]
        self.name = name
        self.href = href
        self.identifier = hash(ensure_unicode(self.href))

    def __str__(self):
        return self.href


class NavMgr(object):
    def __init__(self, env, settings):
        self.settings = settings
        self.env = env

    @cache
    def generate(self, base):
        items = []
        self.do_generate_nav(base, base, items)
        nav_list = self.generate_html(items)
        return self.format_html(nav_list)

    def generate_html(self, items):
        nav_list = []
        for i, item in enumerate(items):
            tmp_list = nav_list
            for j, split in enumerate(item.path):
                idx = self.index_nav(split, tmp_list)
                if idx == -1:
                    if j == len(item.path) - 1:
                        tmp_list.append(item)
                    else:
                        tmp_list.append({split: []})
                        tmp_list = tmp_list[idx][split]
                else:
                    tmp_list = tmp_list[idx][split]
        return nav_list

    def format_html(self, nav_list):
        html = ''
        for nav in nav_list:
            if isinstance(nav, dict):
                for k, v in nav.iteritems():
                    content = self.format_ul(self.format_html(v))
                    html += self.format_li(self.format_span(self._normalize_span(k)) + content)
            else:
                html += self.format_li(self.format_link(nav.name, nav.href, nav.identifier))
        return html

    def _normalize_span(self, content):
        return content.split('.', 1)[1]

    def format_span(self, content):
        tpl = self.env.get_template('nav_title.html')
        return tpl.render(content=ensure_unicode(content))

    def format_ul(self, content):
        tpl = self.env.get_template('nav_ul.html')
        return tpl.render(content=ensure_unicode(content))

    def format_li(self, content):
        tpl = self.env.get_template('nav_li.html')
        return tpl.render(content=ensure_unicode(content))

    def format_link(self, content, link, identifier):
        tpl = self.env.get_template('nav_link.html')
        return tpl.render(content=ensure_unicode(content), link=ensure_unicode(link), identifier=identifier)

    def index_nav(self, name, nav_list):
        for i, item in enumerate(nav_list):
            if isinstance(item, dict) and name in item:
                return i
        return -1

    def do_generate_nav(self, root, base, result):
        names = os.listdir(base)

        folders = []
        files = []
        for name in names:
            path = os.path.join(base, name)
            if os.path.isdir(path):
                folders.append(path)
            else:
                if name.endswith('.md'):
                    files.append(path)

        folders.sort()
        for folder in folders:
            self.do_generate_nav(root, folder, result)

        mds = []
        for path in files:
            lines = open(path).readlines()
            timestamp = lines[1].split(':', 1)[1].strip()
            href = lines[4].split(':', 1)[1].strip()
            name = lines[0].split(':', 1)[1].strip()
            mds.append((path, timestamp, href, name))

        order = self.settings.get('BOOK_ORDER', None)
        if order == 'name':
            mds.sort(key=lambda x: x[3], reverse=False)
        else:
            mds.sort(key=lambda x: x[1], reverse=True)

        for path, _, href, name in mds:
            path = path.replace('\\', '/').replace(root.replace('\\', '/'), '')
            result.append(NavItem(path, href, name))
