# -*- encoding:utf-8 -*-

from plasma.generator.base_generator import BaseGenerator
from plasma.generator.nav_mgr import NavMgr


class ArticleGenerator(BaseGenerator):
    def __init__(self, env, settings):
        super(ArticleGenerator, self).__init__(env, settings)
        self.nav_mgr = NavMgr(env, settings)

    def generate(self, article_list):
        if self.settings['IS_BOOK']:
            nav = self.nav_mgr.generate(self.settings['SOURCE_FOLDER'])
        else:
            nav = ''
        for article in article_list:
            folder = article.url[1:] if article.url.startswith('/') else article.url
            self.render(folder, article=article, nav=nav)

    def get_template_name(self):
        return 'article.html'
