# -*- encoding:utf-8 -*-

from plasma.utils.encoding_utils import ensure_utf8
from plasma.utils.util_funcs import make_dir
import os


class BaseGenerator(object):

    def __init__(self, env, settings):
        self.env = env
        self.settings = settings

    def generate(self, article_list):
        pass

    def render(self, path, **kwargs):
        template = self.env.get_template(self.get_template_name())
        kwargs['settings'] = self.settings
        html = template.render(**kwargs)
        self.write(path, html)

    def get_template_name(self):
        pass

    def write(self, path, content):
        folder = os.path.join(self.settings['PUBLIC_FOLDER'], path)
        make_dir(folder)
        with open(os.path.join(folder, 'index.html'), 'w') as outputs:
            outputs.write(ensure_utf8(content))
            outputs.flush()
