# -*- encoding:utf-8 -*-

from plasma.generator.base_generator import BaseGenerator
from plasma.utils.util_funcs import chunks, escape_char
import os
import shutil


class ArticleListGenerator(BaseGenerator):

    def __init__(self, env, settings):
        super(ArticleListGenerator, self).__init__(env, settings)

    def generate(self, article_list):
        group_dict = self.build_group_dict(article_list)
        for key, group_list in group_dict.iteritems():
            page_list = self.build_page_list(group_list)
            base = self.get_base()
            escape_key = escape_char(key)
            folder = os.path.join(base, escape_key)
            total = len(page_list)
            if self.get_base():
                base_url = '/%s/%s/page/' % (self.get_base(), escape_key)
            else:
                base_url = '/page/'
            for idx, item_list in enumerate(page_list):
                page_info = {
                    'page': idx + 1,
                    'total': total,
                    'base_url': base_url,
                }
                self.generate_page(key, folder, idx + 1, item_list, page_info)
            if page_list:
                src = os.path.join(self.settings['PUBLIC_FOLDER'], folder, 'page/1/index.html')
                dst = os.path.join(self.settings['PUBLIC_FOLDER'], folder, 'index.html')
                shutil.copy2(src, dst)

    def build_group_dict(self, article_list):
        return {}

    def build_page_list(self, article_list):
        page_list = chunks(article_list, self.settings['PAGE_NUM'])
        return page_list

    def generate_page(self, key, folder, page, article_list, page_info):
        path = os.path.join(folder, 'page', str(page))
        kwargs = {'article_list': article_list, 'name': key, 'page_info': page_info}
        kwargs.update(self.get_kwargs())
        self.render(path, **kwargs)

    def get_kwargs(self):
        return {}

    def get_base(self):
        return ''

