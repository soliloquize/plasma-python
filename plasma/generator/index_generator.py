# -*- encoding:utf-8 -*-

from plasma.generator.article_list_generator import ArticleListGenerator
import os
import shutil


class IndexGenerator(ArticleListGenerator):

    def __init__(self, env, settings):
        super(IndexGenerator, self).__init__(env, settings)

    def build_group_dict(self, article_list):
        return {'': article_list}

    def get_base(self):
        return ''

    def get_template_name(self):
        return 'index.html'

    def generate(self, article_list):
        if self.settings['IS_BOOK']:
            self._copy_latest_article(article_list)
        else:
            return super(IndexGenerator, self).generate(article_list)

    def _copy_latest_article(self, article_list):
        article_list.sort(key=lambda x: x.slug, reverse=True)
        if not article_list:
            return
        article = article_list[0]
        path = os.path.join(self.settings['PUBLIC_FOLDER'], article.slug[1:], 'index.html')
        shutil.copy(path, os.path.join(self.settings['PUBLIC_FOLDER'], 'index.html'))
