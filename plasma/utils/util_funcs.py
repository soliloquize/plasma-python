# -*- encoding:utf-8 -*-
import os
import re
from plasma.utils.encoding_utils import ensure_unicode, ensure_utf8


def init_from_dict(instance, info_dict):
    for key, value in info_dict.iteritems():
        if hasattr(instance, key):
            setattr(instance, key, value)


def escape_char(content):
    content = ensure_unicode(content)
    content = re.sub(' +', '__', content)
    content = re.sub('\W', '', content, flags=re.UNICODE)
    content = re.sub('__', '-', content)
    content = re.sub('-+', '-', content)
    content = content.replace('.', '')
    content = content.replace('"', '')
    content = content.replace("'", '')
    return content


def make_dir(path):
    if os.path.exists(path):
        return
    os.makedirs(path)


def chunks(seq, n):
    return [seq[i: i + n] for i in xrange(0, len(seq), n)]

