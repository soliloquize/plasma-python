# -*- encoding:utf-8 -*-

from functools import wraps


CACHE_INFO = {}


def cache(func):
    @wraps(func)
    def _(*args, **kwargs):
        key = func.__name__ + str(args) + str(kwargs)
        if key not in CACHE_INFO:
            CACHE_INFO[key] = func(*args, **kwargs)
        return CACHE_INFO[key]
    return _
