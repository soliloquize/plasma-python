# -*- encoding:utf-8 -*-

import datetime


def str_to_datetime(content, format='%Y-%m-%d %H:%M:%S'):
    return datetime.datetime.strptime(content, format)


def datetime_to_str(content, format='%Y-%m-%d %H:%M:%S'):
    return content.strftime(format)


def reformat(content, from_format, to_format):
    return datetime_to_str(str_to_datetime(content, from_format), to_format)

