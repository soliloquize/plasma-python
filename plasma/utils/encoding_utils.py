# -*- encoding:utf-8 -*-


def ensure_utf8(content, charset='utf-8'):
    if isinstance(content, unicode):
        return content.encode('utf-8')
    return content.decode(charset).encode('utf-8')


def ensure_unicode(content, charset='utf-8'):
    if isinstance(content, unicode):
        return content
    return content.decode(charset)
