#!/usr/bin/env python
# -*- encoding:utf-8 -*-

import os
import sys
import datetime
import inspect
import shutil
import time
from jinja2 import Template
import plasma

sys.path.append(os.path.abspath('.'))

try:
    import config
except:
    pass

from plasma.utils.util_funcs import escape_char
from plasma.utils.encoding_utils import ensure_utf8, ensure_unicode
from plasma.core.workflow import Workflow


def init():
    _generate_config()
    _generate_data_folder()


def fab():
    _generate_fabfile()


def _generate_config():
    folder = os.path.dirname(plasma.__file__)
    config_path = os.path.join(folder, 'static/template/config.py.tpl')
    with open(config_path) as inputs:
        tpl = inputs.read()
    themes = os.path.join(folder, 'static/themes')
    base = os.path.abspath('.')
    content = tpl.format(themes=themes, base=base)
    with open('./config.py', 'w') as outputs:
        outputs.write(content)
        outputs.flush()


def _generate_fabfile():
    folder = os.path.dirname(plasma.__file__)
    config_path = os.path.join(folder, 'static/template/fabfile.py.tpl')
    with open(config_path) as inputs:
        tpl = inputs.read()
    work_dir = os.path.abspath('.')
    server_name = config.SERVER_NAME
    remote_path = config.REMOTE_PATH
    content = tpl.format(work_dir=work_dir, server_name=server_name, remote_path=remote_path)
    with open('./fabfile.py', 'w') as outputs:
        outputs.write(content)
        outputs.flush()


def _generate_data_folder():
    if os.path.exists('sources'):
        return
    os.makedirs('sources/posts')
    os.makedirs('sources/drafts')
    os.makedirs('sources/public_image')
    os.makedirs('sources/public_file')


def create(name):
    folder = os.path.dirname(plasma.__file__)
    article_path = os.path.join(folder, 'static/template/article.md')
    template = Template(open(article_path).read())
    now = datetime.datetime.now()
    ts = now.strftime('%Y-%m-%d %H:%M:%S')
    slug = '/%s/%s/' % (now.strftime('%Y/%m/%d'), ensure_utf8(escape_char(name)))
    res = template.render(title=ensure_unicode(name), timestamp=ts, slug=ensure_unicode(slug))
    filename = '%s.md' % ensure_utf8(escape_char(name))
    path = os.path.join('sources/posts', filename)
    with open(path, 'w') as outputs:
        outputs.write(ensure_utf8(res))
        outputs.flush()
    print 'create', path


def generate():
    start = time.time()
    settings = {key: value for key, value in inspect.getmembers(config)}
    workflow = Workflow()
    workflow.execute(settings)
    copy_public_image()
    copy_public_file()
    end = time.time()
    print 'generate, cost: %s' % (end - start)


def copy_public_image():
    folder = config.IMAGE_FOLDER[config.IMAGE_FOLDER.rindex('/') + 1:]
    dst = os.path.join(config.PUBLIC_FOLDER, folder)
    _copy_folder(config.IMAGE_FOLDER, dst)


def copy_public_file():
    folder = config.FILE_FOLDER[config.FILE_FOLDER.rindex('/') + 1:]
    dst = os.path.join(config.PUBLIC_FOLDER, folder)
    _copy_folder(config.FILE_FOLDER, dst)


def _copy_folder(source_folder, dst_folder):
    if os.path.exists(dst_folder):
        shutil.rmtree(dst_folder)
    shutil.copytree(source_folder, dst_folder)


if __name__ == '__main__':
    command = sys.argv[1]
    if command == 'init':
        init()
    elif command == 'fab':
        fab()
    elif command == 'new':
        create(sys.argv[2].strip())
    elif command == 'generate':
        generate()
